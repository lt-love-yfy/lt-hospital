package com.lt.hospital.service;

import com.lt.hospital.model.hosp.Hospital;
import com.lt.hospital.vo.hosp.HospitalQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface HospitalService {
    /**
     * 提交医院信息
     * @param stringObjectMap
     */
    void save(Map<String, Object> stringObjectMap);

    /**
     * 根据hoscode查询医院
     * @param hoscode
     * @return
     */
    Hospital getHospitalByHoscode(String hoscode);

    /**
     * 分页查询
     * @param page
     * @param limit
     * @param hospitalQueryVo
     * @return
     */
    Page<Hospital> findPageHospital(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo);

    /**
     * 医院状态设置
     * @param id
     * @param status
     */
    void updateStatus(String id, Integer status);

    /**
     * 医院查询操作
     * @param id
     * @return
     */
    Map<String, Object> showHospitalDetail(String id);

    /**
     * 根据医院名称查询医院
     * @param hosname
     * @return
     */
    List<Hospital> getHospitalByHosname(String hosname);

    /**
     * 用户界面需要获取到的医院预约详情
     * @param hoscode
     * @return
     */
    Map<String, Object> item(String hoscode);
}
