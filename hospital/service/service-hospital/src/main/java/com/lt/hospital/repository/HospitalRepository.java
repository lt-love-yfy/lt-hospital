package com.lt.hospital.repository;

import com.lt.hospital.model.hosp.Hospital;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//这个是mongo数据库操作要写的内容 继承的内容里面写入实体类和类型
@Repository
public interface HospitalRepository extends MongoRepository<Hospital,String> {
    //这个方法已经帮我们实现好了  可以看mongodb文档后的说明，按照spring Data规范
    //判断数据是否存在
    Hospital getHospitalByHoscode(String hoscode);
    //根据医院名称的模糊查询
    List<Hospital> getHospitalByHosnameLike(String hosname);
}
