package com.lt.hospital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


//因为配置了mongodb，所以启动时候需要开启
@SpringBootApplication()
//扫描这个类下面的东西，意图就是添加swagger
@ComponentScan(basePackages = "com.lt")
//表示nacos服务的启动
@EnableDiscoveryClient
//找到service_cmn_client模块下的@FeignClient("service-cmn")
@EnableFeignClients(basePackages = "com.lt")
public class ServiceHospitalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceHospitalApplication.class,args);
    }
}
