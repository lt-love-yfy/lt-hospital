package com.lt.hospital.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lt.common.exception.YyghException;
import com.lt.common.result.ResultCodeEnum;
import com.lt.hospital.mapper.HospitalSetMapper;
import com.lt.hospital.model.hosp.HospitalSet;
import com.lt.hospital.service.HospitalSetService;
import com.lt.hospital.vo.order.SignInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//实现类也是mp帮忙写好了的，前面写mapper文件，然后后面就写实体类
@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService{

    //这个其实可以不写的，ServiceImpl<HospitalSetMapper, HospitalSet>在内部已经帮忙写好了 可以看源码
    @Autowired
    private HospitalSetMapper hospitalSetMapper;

    /**
     * 通过hoscode查询key
     * @param hoscode
     * @return
     */
    @Override
    public String getSignByHoscode(String hoscode) {
        //mp 提供条件查询方法
        QueryWrapper<HospitalSet> wrapper =new QueryWrapper<>();
        wrapper.like("hoscode",hoscode);
        //selectOne 查询方式

        HospitalSet hospitalSet=hospitalSetMapper.selectOne(wrapper);
        return hospitalSet.getSignKey();
    }

    /**
     * 获取医院的签名信息
     * @param hoscode
     * @return
     */
    @Override
    public SignInfoVo getSignInfoVo(String hoscode) {
        QueryWrapper<HospitalSet> wrapper = new QueryWrapper<>();
        wrapper.eq("hoscode",hoscode);
        HospitalSet hospitalSet = baseMapper.selectOne(wrapper);

        if(null == hospitalSet) {
            throw new YyghException(ResultCodeEnum.HOSPITAL_OPEN);
        }

        SignInfoVo signInfoVo = new SignInfoVo();
        signInfoVo.setApiUrl(hospitalSet.getApiUrl());
        signInfoVo.setSignKey(hospitalSet.getSignKey());
        return signInfoVo;
    }
}
