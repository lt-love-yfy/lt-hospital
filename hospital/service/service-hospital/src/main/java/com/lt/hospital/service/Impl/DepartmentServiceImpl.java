package com.lt.hospital.service.Impl;


import com.alibaba.fastjson.JSONObject;
import com.lt.hospital.model.hosp.Department;
import com.lt.hospital.repository.DepartmentRepository;
import com.lt.hospital.service.DepartmentService;
import com.lt.hospital.vo.hosp.DepartmentQueryVo;
import com.lt.hospital.vo.hosp.DepartmentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;


    /**
     * 上传科室信息
     * @param stringObjectMap
     */
    @Override
    public void save(Map<String, Object> stringObjectMap) {
        //首先将map对象转换为字符串
        String jsonString = JSONObject.toJSONString(stringObjectMap);
        //再将字符串转换为对象
        Department department = JSONObject.parseObject(jsonString, Department.class);

        //从数据库查询
        String hoscode = department.getHoscode();
        String depcode = department.getDepcode();
        //调用departmentRepository方法
        Department departmentExist = departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode,depcode);

        if (null!=departmentExist){
            department.setUpdateTime(new Date());
            department.setIsDeleted(0);
            departmentRepository.save(department);
        }else {
            department.setCreateTime(new Date());
            department.setUpdateTime(new Date());
            department.setIsDeleted(0);
            departmentRepository.save(department);

        }
    }

    @Override
    public Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo) {

        //创建Pageable对象，这里page和和mysql里面不一样，下标从0开始，所以设置时候要将当前页减去1
        Pageable pageable = PageRequest.of(page-1,limit);

        //创建Department对象
        Department department = new Department();
        //将departmentQueryVo对象转换为department对象
        BeanUtils.copyProperties(departmentQueryVo, department);
        department.setIsDeleted(0);

        //创建Example对象，这是mongo里面的写法
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING) //改变默认字符,串匹配方式：模糊查询
                .withIgnoreCase(true); //改变默认大小写忽略方式：忽略大小写

        //创建实例
        Example<Department> example = Example.of(department, matcher);

        Page<Department> all = departmentRepository.findAll(example, pageable);
        return all;
    }

    @Override
    public void remove(String hoscode, String depcode) {
        //查询是否存在这个科室
        Department departmentExist = departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode,depcode);
        if (null!=departmentExist){
            departmentRepository.deleteById(departmentExist.getId());
        }
    }

    /**
     * 科室信息查询
     * @param hoscode
     * @return
     */
    @Override
    public List<DepartmentVo> findDepartmentTree(String hoscode) {
        //前端界面是一个树形界面，三个组件，而科室对应的有大科室下面嵌套的有小科室的存在，所以需要遍历
        //创建一个科室对象
        List<DepartmentVo> result = new ArrayList<>();

        //通过医院编号查询到所有的科室信息
        Department departmentQuery =new Department();
        departmentQuery.setHoscode(hoscode);
        Example example =Example.of(departmentQuery);
        //然后得到科室的列表
        List<Department> departmentList =departmentRepository.findAll(example);

        //通过大科室编号getBigcode分组，因为大科室下面还有一些小科室
        Map<String, List<Department>> collect = departmentList.stream().collect(Collectors.groupingBy(Department::getBigcode));
        //遍历map集合
        for(Map.Entry<String,List<Department>> entry :collect.entrySet()){
            //首先得到大科室的编号
            String bigcode = entry.getKey();
            //大科室编号对应的全局数据
            List<Department> AllDepartmentList = entry.getValue();

            //大科室数据的封装
            DepartmentVo BigDepartmentVo = new DepartmentVo();
            BigDepartmentVo.setDepcode(bigcode);
            //get(0)代表的就是说第一个就是大科室本身
            BigDepartmentVo.setDepname(AllDepartmentList.get(0).getBigname());

            //大科室封装完了，小科室,深入理解这个业务逻辑代码的描写
            List<DepartmentVo> SmallDepartmentList = new ArrayList<>();

            for(Department department : AllDepartmentList){
                DepartmentVo departmentVo1 =new DepartmentVo();
                departmentVo1.setDepcode(department.getDepcode());
                departmentVo1.setDepname(department.getDepname());
                //封装到list集合
                SmallDepartmentList.add(departmentVo1);
            }
            //把小科室list集合放到大科室children里面
            BigDepartmentVo.setChildren(SmallDepartmentList);
            result.add(BigDepartmentVo);
        }

        return result;
    }

    /**
     * 科室名称查询
     * @param hoscode
     * @param depcode
     * @return
     */
    @Override
    public String  getDepName(String hoscode, String depcode) {
        Department department =departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode, depcode);
        if (department!=null){
            return department.getDepname();
        }
        return null;
    }

    /**
     * 通过医院编号和科室编号获取科室详细信息
     * @param hoscode
     * @param depcode
     * @return
     */
    @Override
    public Department getDepartment(String hoscode, String depcode) {
        return departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode,depcode);
    }
}
