package com.lt.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lt.hospital.model.hosp.HospitalSet;
import com.lt.hospital.vo.order.SignInfoVo;

//这里相当于mybatis plus帮忙写好了，继承了一个IService泛型 里面填写的就是那个实体类
public interface HospitalSetService extends IService<HospitalSet> {
    /**
     * 通过hoscode查询key
     * @param hoscode
     * @return
     */
    String getSignByHoscode(String hoscode);

    /**
     * 获取医院签名信息
     * @param hoscode
     * @return
     */
    SignInfoVo getSignInfoVo(String hoscode);
}
