package com.lt.hospital.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lt.common.exception.YyghException;
import com.lt.common.result.Result;
import com.lt.common.utils.MD5;
import com.lt.hospital.model.hosp.HospitalSet;
import com.lt.hospital.service.HospitalSetService;
import com.lt.hospital.vo.hosp.HospitalSetQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

//swagger注解
//@Api：修饰整个类，描述Controller的作用
//@ApiOperation：描述一个类的一个方法，或者说一个接口
//@ApiParam：单个参数描述
//@ApiModel：用对象来接收参数
//@ApiModelProperty：用对象接收参数时，描述对象的一个字段
//@ApiImplicitParam：一个请求参数
//@ApiImplicitParams：多个请求参数

@Api(tags = "医院管理设置")
@RestController
@RequestMapping("/admin/hospital/hospitalSet")
//因为有了crossConfig配置了，这个注解可以去掉@CrossOrigin //跨域操作，因为前端的端口号和后端不一致，需要加入这个注解，（了解同源的概念）
public class HospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;

    /**
     * 查询所有的医院信息
     * @return
     */
    @GetMapping("findAll")
    @ApiOperation(value = "查询出列表中所有医院信息")
    //这个list方法其实也是mp里面给你提供给好了的方法，直接拿来查询操作的
    public Result findAll(){
        List<HospitalSet> list =hospitalSetService.list();
        Result<List<HospitalSet>> ok = Result.ok(list);
        return ok;
    }


    /**
     * 删除医院
     * @param id 主键
     * @return
     */
    @ApiOperation(value = "删除医院设置")
    @DeleteMapping("{id}")
    public Result removeById(@PathVariable Long id){
        //这也是mp提供的，看看源码，或者配置查看底层sql语句 返回的是Boolean类型
        boolean flag = hospitalSetService.removeById(id);
        return flag ? Result.ok():Result.fail();
    }

    /**
     * 分页条件查询，requestBody 联合POST请求一起提交
     * @param current 当前页
     * @param limit 每页显示几条信息
     * @param hospitalSetQueryVo
     * @return
     */
    @ApiOperation(value = "条件分页查询")
    @PostMapping("queryPageHospital/{current}/{limit}")
    public Result queryPageHospital(@PathVariable Long current, @PathVariable Long limit,
                                    @RequestBody(required = false) HospitalSetQueryVo hospitalSetQueryVo){
        //创建page对象，传递当前页，每页记录数
        Page<HospitalSet> page =new Page<>(current,limit);//如果里面不传入数据那么，current默认为1，size默认为10
        //mp的构建条件
        QueryWrapper<HospitalSet> wrapper =new QueryWrapper<>();
        //获取医院名称和医院编号
        String hosname =hospitalSetQueryVo.getHosname();
        String hoscode =hospitalSetQueryVo.getHoscode();
        //如果传入的值不为空，那么查询，因为数据库表字段中填写的医院名称或者医院编号有可能为空的
        if (!StringUtils.isEmpty(hosname)){
            //医院名称需要模糊查询，第一个参数为字段名，第二个是填写的模糊匹配传值
            wrapper.like("hosname",hosname);
        }
        if (!StringUtils.isEmpty(hoscode)){
            //医院名称需要模糊查询，第一个参数为字段名，第二个是填写的精确匹配传值
            wrapper.eq("hoscode",hoscode);
        }
        //调用方法实现分页查询 page 也是mp封装好的，第一个参数传入上述page对象，第二个传入构建的条件
        Page<HospitalSet> pageHospitalSet = hospitalSetService.page(page, wrapper);
        return Result.ok(pageHospitalSet);
    }

    /**
     * 添加医院操作 swagger写的时候 status 和 密钥基本可以不写了
     * @param hospitalSet 添加的医院类
     * @return
     */
    @ApiOperation(value = "添加医院操作")
    @PostMapping("addHospitalSet")
    public Result addHospitalSet (@RequestBody HospitalSet hospitalSet){
        //设置状态 1 使用 0 不能使用
        hospitalSet.setStatus(1);
        //签名密钥
        Random random = new Random();
        //MD5是写好的加密类
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis()+""+random.nextInt(1000)));
        return hospitalSetService.save(hospitalSet)?Result.ok():Result.fail();
    }

    /**
     * 根据id更改医院设置
     * @param hospitalSet
     * @return
     */
    @ApiOperation(value = "更改医院设置")
    @PostMapping("updateHospitalSet")
    public Result updateHospitalSet(@RequestBody HospitalSet hospitalSet){
        return hospitalSetService.updateById(hospitalSet)?Result.ok():Result.fail();
    }

    /**
     *  根据id查询医院
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询医院")
    @GetMapping("queryHospitalSetById/{id}")
    public Result queryHospitalSetById(@PathVariable Long id){

//        try {
//            int i=1/0;
//        }catch (Exception e){
//            throw new YyghException("失败啦",201);
//        }
        HospitalSet byId = hospitalSetService.getById(id);
        return Result.ok(byId);
    }

    /**
     * 批量删除
     * @param id
     * @return
     */
    @ApiOperation(value = "批量删除操作")
    @DeleteMapping("batchRemoveHospitalSet")
    public Result batchRemoveHospitalSet(@RequestBody List<Long> id){
        hospitalSetService.removeByIds(id);
        return Result.ok();
    }


    /**
     * 医院设置锁定和解锁，很简单的一个方法
     * @param id
     * @param status
     * @return
     */
    //ps:PutMapping与PostMapping区别不大其实
    @ApiOperation(value = "医院设置锁定和解锁")
    @PutMapping("lockHospitalSet/{id}/{status}")
    public Result lockHospitalSet(@PathVariable Long id,
                                  @PathVariable Integer status) {
        //根据id查询医院设置信息
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        //设置状态
        hospitalSet.setStatus(status);
        //调用方法
        hospitalSetService.updateById(hospitalSet);
        return Result.ok();
    }

    /**
     * 签名密钥的发送
     * @param id
     * @return
     */
    @ApiOperation(value = "签名密钥的发送")
    @PutMapping("sendKey/{id}")
    public Result lockHospitalSet(@PathVariable Long id) {
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        String signKey = hospitalSet.getSignKey();
        String hoscode = hospitalSet.getHoscode();
        //TODO 发送短信
        return Result.ok();
    }
}
