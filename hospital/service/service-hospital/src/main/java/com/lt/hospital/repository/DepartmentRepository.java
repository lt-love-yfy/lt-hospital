package com.lt.hospital.repository;

import com.lt.hospital.model.hosp.Department;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends MongoRepository<Department,String> {
    //查询方法
    Department getDepartmentByHoscodeAndDepcode(String hoscode, String depcode);
}
