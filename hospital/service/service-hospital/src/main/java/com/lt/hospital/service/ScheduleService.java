package com.lt.hospital.service;

import com.lt.hospital.model.hosp.Schedule;
import com.lt.hospital.vo.hosp.ScheduleOrderVo;
import com.lt.hospital.vo.hosp.ScheduleQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface ScheduleService {
    /**
     * 排班上传
     * @param stringObjectMap
     */
    void save(Map<String, Object> stringObjectMap);

    /**
     * 分页查询
     * @param page
     * @param limit
     * @param scheduleQueryVo
     * @return
     */
    Page<Schedule> findPageSchedule(int page, int limit, ScheduleQueryVo scheduleQueryVo);

    /**
     * 删除排班操作
     * @param hoscode
     * @param hosScheduleId
     */
    void remove(String hoscode, String hosScheduleId);

    /**
     * 通过医院编号格科室编号，查询排班日期
     * @param page
     * @param limit
     * @param hoscode
     * @param depcode
     * @return
     */
    Map<String, Object> getScheduleList(Integer page, Integer limit, String hoscode, String depcode);

    /**
     * 通过医院编号科室，科室编号，工作日期查询到排班具体信息
     * @param hoscode
     * @param depcode
     * @param workDate
     * @return
     */
    List<Schedule> getScheduleDetailList(String hoscode, String depcode, String workDate);

    /**
     * 获取可预约排班数据
     * @param page
     * @param limit
     * @param hoscode
     * @param depcode
     * @return
     */
    Map<String,Object> getBookingScheduleRule(Integer page, Integer limit, String hoscode, String depcode);

    /**
     * 根基排班id获取到具体的排班信息
     * @param scheduleId
     * @return
     */
    Schedule getScheduleId(String scheduleId);

    /**
     * 根据排班id获取预约下单数据
     * @param scheduleId
     * @return
     */
    ScheduleOrderVo getScheduleOrderVo(String scheduleId);
}
