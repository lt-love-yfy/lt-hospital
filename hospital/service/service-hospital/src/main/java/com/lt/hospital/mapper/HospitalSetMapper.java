package com.lt.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lt.hospital.model.hosp.HospitalSet;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
