package com.lt.hospital.service;


import com.lt.hospital.model.hosp.Department;
import com.lt.hospital.vo.hosp.DepartmentQueryVo;
import com.lt.hospital.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DepartmentService {
    /**
     * 上传科室信息
     * @param stringObjectMap
     */
    void save(Map<String, Object> stringObjectMap);

    /**
     * 分页条件查询
     * @param page
     * @param limit
     * @param departmentQueryVo
     * @return
     */
    Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo);

    /**
     *  科室删除
     * @param hoscode
     * @param depcode
     * @return
     */
    void remove(String hoscode, String depcode);

    /**
     * 查询科室信息
     * @param hoscode
     * @return
     */
    List<DepartmentVo> findDepartmentTree(String hoscode);

    /**
     * 通过医院科室编号查询科室名称
     * @param hoscode
     * @param depcode
     * @return
     */
    String getDepName(String hoscode, String depcode);

    /**
     * 通过医院编号和科室编号获取科室详细信息
     * @param hoscode
     * @param depcode
     * @return
     */
    Department getDepartment(String hoscode, String depcode);
}
