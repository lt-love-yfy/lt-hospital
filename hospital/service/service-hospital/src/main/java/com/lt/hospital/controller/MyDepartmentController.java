package com.lt.hospital.controller;


import com.lt.common.result.Result;
import com.lt.hospital.service.DepartmentService;
import com.lt.hospital.vo.hosp.DepartmentVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "科室设置")
@RestController
@RequestMapping("/admin/hospital/department")
//因为有了crossConfig配置了，这个注解可以去掉@CrossOrigin
public class MyDepartmentController {

    @Autowired
    private DepartmentService departmentService;


    @ApiOperation("医院科室查询")
    @GetMapping("departmentList/{hoscode}")
    public Result departmentList(@PathVariable String hoscode){
        List<DepartmentVo> list =departmentService.findDepartmentTree(hoscode);
        return Result.ok(list);
    }
}
