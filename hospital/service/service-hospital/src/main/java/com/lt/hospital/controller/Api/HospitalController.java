package com.lt.hospital.controller.Api;

import com.lt.common.exception.YyghException;
import com.lt.common.helper.HttpRequestHelper;
import com.lt.common.result.Result;
import com.lt.common.result.ResultCodeEnum;
import com.lt.common.utils.MD5;
import com.lt.hospital.model.hosp.Hospital;
import com.lt.hospital.service.HospitalService;
import com.lt.hospital.service.HospitalSetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "医院接口调用设置")
@RestController
//这个需要与hospital-manage里面的路径统一才行
@RequestMapping("/api/hosp")
public class HospitalController {

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    @ApiOperation(value = "医院查询接口")
    @PostMapping("hospital/show")
    public Result queryHospital(HttpServletRequest httpServletRequest){
        //传递到此的医院信息
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        Map<String, Object> stringObjectMap = HttpRequestHelper.switchMap(parameterMap);

        //1.首先需要取得传来的密钥，ApiServiceImpl帮我们前端传来的值，MD5已经帮忙加密了
        String sign = (String) stringObjectMap.get("sign"); //将object强转为string

        //2.其次通过hoscode查询从而拿到sign值,然后再通过MD5进行二次加密
        String hoscode = (String) stringObjectMap.get("hoscode");

        String signByHoscode = hospitalSetService.getSignByHoscode(hoscode);

        String encrypt = MD5.encrypt(signByHoscode);

        //3.最后进行签名判断是否一致
        if(!sign.equals(encrypt)){
            throw new  YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        Hospital hospital=hospitalService.getHospitalByHoscode(hoscode);
        return Result.ok(hospital);
    }

    //医院上传接口
    //根据Java规范：request.getParameterMap()返回的是一个Map类型的值，
    // 该返回值记录着前端（如jsp页面）所提交请求中的请求参数和请求参数值的映射关系。
    // 这个返回值有个特别之处——只能读，不像普通的Map类型数据一样可以修改。
    // 这是因为服务器为了实现一定的安全规范，所作的限制，如果需要修改那么需要新建一个map对象去复制上面的值进行修改。
    @ApiOperation(value = "医院上传接口")
    @PostMapping("saveHospital")
    public Result saveHospital(HttpServletRequest httpServletRequest){
        //传递到此的医院信息
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        //将map中的string[]转换为Object[],利用写好的工具类
        Map<String, Object> stringObjectMap = HttpRequestHelper.switchMap(parameterMap);

        //需要考虑只有特定的医院才能调用特定的接口，所以通过对比取到的密钥是否相同来进行判断

        //1.首先需要取得传来的密钥，ApiServiceImpl帮我们前端传来的值，MD5已经帮忙加密了
        String sign = (String) stringObjectMap.get("sign"); //将object强转为string

        //2.其次通过hoscode查询从而拿到sign值,然后再通过MD5进行二次加密
        String hoscode = (String) stringObjectMap.get("hoscode");

        String signByHoscode = hospitalSetService.getSignByHoscode(hoscode);

        String encrypt = MD5.encrypt(signByHoscode);

        //3.最后进行签名判断是否一致
        if(!sign.equals(encrypt)){
            throw new  YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //因为logoData是一个图片，传输时候“+”变成了“ ”，现在读取要转换回来
        String logoData = (String) stringObjectMap.get("logoData");
        logoData =logoData.replaceAll(" ","+");
        stringObjectMap.put("logoData",logoData);

        hospitalService.save(stringObjectMap);
        return Result.ok();
    }
}
