package com.lt.hospital.controller.UserApi;


import com.lt.common.result.Result;
import com.lt.hospital.model.hosp.Hospital;
import com.lt.hospital.model.hosp.Schedule;
import com.lt.hospital.service.DepartmentService;
import com.lt.hospital.service.HospitalService;
import com.lt.hospital.service.HospitalSetService;
import com.lt.hospital.service.ScheduleService;
import com.lt.hospital.vo.hosp.DepartmentVo;
import com.lt.hospital.vo.hosp.HospitalQueryVo;
import com.lt.hospital.vo.hosp.ScheduleOrderVo;
import com.lt.hospital.vo.order.SignInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "医院管理接口")
@RequestMapping("/api/hospital/user") //hospital只能写在第二个，因为网关配置跨域下扫描到的就是这个格式spring.cloud.gateway.routes[0].predicates= Path=/*/hospital/**
@RestController
public class HospitalApiController {

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private HospitalSetService hospitalSetService;


    @ApiOperation(value = "医院列表查询")
    @GetMapping("findHospitalList/{page}/{limit}")
    public Result findHospitalList(@PathVariable Integer page,
                                    @PathVariable Integer limit,
                                    HospitalQueryVo hospitalQueryVo){

        Page<Hospital> pageModel = hospitalService.findPageHospital(page, limit, hospitalQueryVo);

        List<Hospital> content = pageModel.getContent();//获取当前页下的内容
        int totalPages = pageModel.getTotalPages();//获取总页数
        long totalElements = pageModel.getTotalElements();//获得总记录数

        return Result.ok(pageModel);
    }
    @ApiOperation(value = "根据医院名称查询")
    @GetMapping("findHospitalByName/{hosname}")
    public Result findHospitalByName(@PathVariable String hosname){

        List<Hospital> hospitalList=hospitalService.getHospitalByHosname(hosname);
        return Result.ok(hospitalList);
    }
    @ApiOperation(value = "根据医院编号获取科室列表")
    @GetMapping("department/{hoscode}")
    public Result index(
            @ApiParam(name = "hoscode", value = "医院code", required = true)
            @PathVariable String hoscode) {

        List<DepartmentVo> departmentTree = departmentService.findDepartmentTree(hoscode);
        return Result.ok(departmentTree);
    }


    @ApiOperation(value = "医院预约挂号详情")
    @GetMapping("findHospitalDetail/{hoscode}")
    public Result item(@ApiParam(name = "hoscode", value = "医院code", required = true) @PathVariable String hoscode) {
        Map<String, Object> map=hospitalService.item(hoscode);
        return Result.ok(map);
    }

    @ApiOperation(value = "获取可预约排班数据")
    @GetMapping("auth/getBookingScheduleRule/{page}/{limit}/{hoscode}/{depcode}")
    public Result getBookingSchedule(
            @ApiParam(name = "page", value = "当前页码", required = true) @PathVariable Integer page,
            @ApiParam(name = "limit", value = "每页记录数", required = true) @PathVariable Integer limit,
            @ApiParam(name = "hoscode", value = "医院code", required = true) @PathVariable String hoscode,
            @ApiParam(name = "depcode", value = "科室code", required = true) @PathVariable String depcode) {

        return Result.ok(scheduleService.getBookingScheduleRule(page, limit, hoscode, depcode));
    }

    @ApiOperation(value = "获取排班数据")
    @GetMapping("auth/findScheduleList/{hoscode}/{depcode}/{workDate}")
    public Result findScheduleList(
            @ApiParam(name = "hoscode", value = "医院code", required = true) @PathVariable String hoscode,
            @ApiParam(name = "depcode", value = "科室code", required = true) @PathVariable String depcode,
            @ApiParam(name = "workDate", value = "排班日期", required = true) @PathVariable String workDate) {

        return Result.ok(scheduleService.getScheduleDetailList(hoscode, depcode, workDate));
    }

    @ApiOperation(value = "根据排班id获取排班数据")
    @GetMapping("getSchedule/{scheduleId}")
    public Result getSchedule(
            @ApiParam(name = "scheduleId", value = "排班id", required = true)
            @PathVariable String scheduleId) {
        Schedule schedule=scheduleService.getScheduleId(scheduleId);
        return Result.ok(schedule);
    }

    @ApiOperation(value = "根据排班id获取预约下单数据")
    @GetMapping("inner/getScheduleOrderVo/{scheduleId}")
    public ScheduleOrderVo getScheduleOrderVo(
            @ApiParam(name = "scheduleId", value = "排班id", required = true)
            @PathVariable("scheduleId") String scheduleId) {
        return scheduleService.getScheduleOrderVo(scheduleId);
    }

    @ApiOperation(value = "获取医院签名信息")
    @GetMapping("inner/getSignInfoVo/{hoscode}")
    public SignInfoVo getSignInfoVo(
            @ApiParam(name = "hoscode", value = "医院code", required = true)
            @PathVariable("hoscode") String hoscode) {
        return hospitalSetService.getSignInfoVo(hoscode);
    }

}
