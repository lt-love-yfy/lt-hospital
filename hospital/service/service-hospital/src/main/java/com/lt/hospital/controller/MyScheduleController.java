package com.lt.hospital.controller;

import com.lt.common.result.Result;
import com.lt.hospital.model.hosp.Schedule;
import com.lt.hospital.service.ScheduleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags = "排班设置")
@RestController
@RequestMapping("/admin/hospital/schedule")
//因为有了crossConfig配置了，这个注解可以去掉@CrossOrigin
public class MyScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation("医院排班规则查询")
    @GetMapping("scheduleList/{page}/{limit}/{hoscode}/{depcode}")
    public Result scheduleList(@PathVariable Integer page,
                               @PathVariable Integer limit,
                               @PathVariable String hoscode,
                               @PathVariable String depcode){
        Map<String,Object> scheduleMap=scheduleService.getScheduleList(page,limit,hoscode,depcode);
        return Result.ok(scheduleMap);
    }
    @ApiOperation("医院排班具体信息查询")
    @GetMapping("scheduleDetailList/{hoscode}/{depcode}/{workDate}")
    public Result scheduleDetailList(@PathVariable String hoscode,
                                    @PathVariable String depcode,
                                    @PathVariable String workDate){
        List<Schedule> scheduleDetailList=scheduleService.getScheduleDetailList(hoscode,depcode,workDate);
        return Result.ok(scheduleDetailList);
    }

}
