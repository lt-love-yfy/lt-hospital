package com.lt.hospital.controller.Api;

import com.lt.common.exception.YyghException;
import com.lt.common.helper.HttpRequestHelper;
import com.lt.common.result.Result;
import com.lt.common.result.ResultCodeEnum;
import com.lt.common.utils.MD5;
import com.lt.hospital.model.hosp.Schedule;
import com.lt.hospital.service.HospitalSetService;
import com.lt.hospital.service.ScheduleService;
import com.lt.hospital.vo.hosp.ScheduleQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "排班接口调用设置")
@RestController
//这个需要与hospital-manage里面的路径统一才行,查看接口文档就可以
@RequestMapping("/api/hosp")
public class ScheduleController {

    @Autowired
    private HospitalSetService hospitalSetService;

    @Autowired
    private ScheduleService scheduleService;


    @ApiOperation(value = "排班上传接口")
    @PostMapping("saveSchedule")
    public Result saveSchedule(HttpServletRequest httpServletRequest){
        //传递到此的医院信息
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        Map<String, Object> stringObjectMap = HttpRequestHelper.switchMap(parameterMap);

        //1.首先需要取得传来的密钥，ApiServiceImpl帮我们前端传来的值，MD5已经帮忙加密了
        String sign = (String) stringObjectMap.get("sign"); //将object强转为string

        //2.其次通过hoscode查询从而拿到sign值,然后再通过MD5进行二次加密
        String hoscode = (String) stringObjectMap.get("hoscode");

        String signByHoscode = hospitalSetService.getSignByHoscode(hoscode);

        String encrypt = MD5.encrypt(signByHoscode);

        //3.最后进行签名判断是否一致
        if(!sign.equals(encrypt)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        scheduleService.save(stringObjectMap);
        return Result.ok();
    }

    @ApiOperation(value = "排班查询接口")
    @PostMapping("schedule/list")
    public Result queryDepartment(HttpServletRequest httpServletRequest) {
        //传递到此的医院信息
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        Map<String, Object> stringObjectMap = HttpRequestHelper.switchMap(parameterMap);

        //记住将object可以强转为string，不可直接转为integer，从string转为integer
        //从接口调用的信息当中page是否为空，为空，当前页为1，否则设置传来的页码
        int page =  StringUtils.isEmpty(stringObjectMap.get("page")) ?
                1 : Integer.parseInt((String)stringObjectMap.get("page"));
        //从接口调用的信息当中limit是否为空，为空，每页显示10条记录，否则设置传来的显示页数
        int limit = StringUtils.isEmpty(stringObjectMap.get("limit")) ?
                10 : Integer.parseInt((String)stringObjectMap.get("limit"));

        String hoscode = (String) stringObjectMap.get("hoscode");
        String depcode = (String) stringObjectMap.get("depcode");
        //如果为空，那么抛出异常
        if(StringUtils.isEmpty(hoscode)) {
            throw new YyghException(ResultCodeEnum.PARAM_ERROR);
        }

        //签名校验
        String sign = (String) stringObjectMap.get("sign");
        String signByHoscode = hospitalSetService.getSignByHoscode(hoscode);
        String encrypt = MD5.encrypt(signByHoscode);
        if(!sign.equals(encrypt)){
            throw new  YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        ScheduleQueryVo scheduleQueryVo =new ScheduleQueryVo();
        scheduleQueryVo.setHoscode(hoscode);
        scheduleQueryVo.setDepcode(depcode);

        //
        Page<Schedule> pageModel = scheduleService.findPageSchedule(page, limit, scheduleQueryVo);
        return Result.ok(pageModel);
    }
    @ApiOperation(value = "排班删除接口")
    @PostMapping("schedule/remove")
    public Result removeSchedule(HttpServletRequest httpServletRequest) {
        //传递到此的医院信息
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        Map<String, Object> stringObjectMap = HttpRequestHelper.switchMap(parameterMap);

        String hoscode = (String) stringObjectMap.get("hoscode");
        String hosScheduleId = (String) stringObjectMap.get("hosScheduleId");

        //如果为空，那么抛出异常
        if(StringUtils.isEmpty(hoscode)) {
            throw new YyghException(ResultCodeEnum.PARAM_ERROR);
        }
        //签名校验
        String sign = (String) stringObjectMap.get("sign");
        String signByHoscode = hospitalSetService.getSignByHoscode(hoscode);
        String encrypt = MD5.encrypt(signByHoscode);
        if(!sign.equals(encrypt)){
            throw new  YyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //根据医院编号和排班定点删除
        scheduleService.remove(hoscode,hosScheduleId);
        return Result.ok();

    }
}
