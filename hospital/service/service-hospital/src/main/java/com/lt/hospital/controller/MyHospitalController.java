package com.lt.hospital.controller;

import com.lt.common.result.Result;
import com.lt.hospital.model.hosp.Hospital;
import com.lt.hospital.service.HospitalService;
import com.lt.hospital.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags = "医院查看设置")
@RestController
@RequestMapping("/admin/hospital")
//因为有了crossConfig配置了，这个注解可以去掉@CrossOrigin //跨域操作，因为前端的端口号和后端不一致，需要加入这个注解，（了解同源的概念）
public class MyHospitalController {

    @Autowired
    HospitalService hospitalService;

    @ApiOperation(value = "条件分页查询")
    @GetMapping("queryPageMyHospital/{page}/{limit}")
    public Result queryPageHospital(@PathVariable Integer page,
                                    @PathVariable Integer limit,
                                     HospitalQueryVo hospitalQueryVo){

        Page<Hospital> pageModel = hospitalService.findPageHospital(page, limit, hospitalQueryVo);

        List<Hospital> content = pageModel.getContent();//获取当前页下的内容
        int totalPages = pageModel.getTotalPages();//获取总页数
        long totalElements = pageModel.getTotalElements();//获得总记录数
        
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "医院上线状态")
    @PostMapping("updateHospitalStatus/{id}/{status}")
    public Result updateHospitalStatus(@PathVariable String id,
                                  @PathVariable Integer status) {
        hospitalService.updateStatus(id,status);
        return Result.ok();
    }

    @ApiOperation(value = "医院详情信息")
    @GetMapping("showHospitalDetail/{id}")
    public Result showHospitalDetail(@PathVariable String id) {
        Map<String, Object> map=hospitalService.showHospitalDetail(id);
        return Result.ok(map);
    }

}
