package com.lt.oss.sevice;

import org.springframework.web.multipart.MultipartFile;


public interface FileService {
    /**
     * 返回地址
     * @param file
     * @return
     */
    String upload(MultipartFile file);
}
