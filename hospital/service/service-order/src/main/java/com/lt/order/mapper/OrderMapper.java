package com.lt.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lt.hospital.model.order.OrderInfo;

public interface OrderMapper extends BaseMapper<OrderInfo> {
}
