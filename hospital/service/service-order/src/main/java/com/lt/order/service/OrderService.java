package com.lt.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lt.hospital.model.order.OrderInfo;

public interface OrderService extends IService<OrderInfo> {
    //保存订单
    Long saveOrder(String scheduleId, Long patientId);
}
