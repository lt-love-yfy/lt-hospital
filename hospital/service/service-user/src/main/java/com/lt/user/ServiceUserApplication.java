package com.lt.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication()
//扫描这个类下面的东西，意图就是添加swagger
@ComponentScan(basePackages = "com.lt")
//表示nacos服务的启动
@EnableDiscoveryClient
//找到service_cmn_client模块下的@FeignClient("service-cmn")
@EnableFeignClients(basePackages = "com.lt")
public class ServiceUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceUserApplication.class, args);
    }
}

