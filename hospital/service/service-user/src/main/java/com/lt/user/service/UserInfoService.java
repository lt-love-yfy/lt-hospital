package com.lt.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lt.hospital.model.user.UserInfo;
import com.lt.hospital.vo.user.LoginVo;
import com.lt.hospital.vo.user.UserAuthVo;
import com.lt.hospital.vo.user.UserInfoQueryVo;

import java.util.Map;

public interface UserInfoService extends IService<UserInfo> {
    Map<String, Object> login(LoginVo loginVo);

    /**
     * 根据openid判断数据库是否已经含有用户信息
     * @param openid
     * @return
     */
    UserInfo selectWxInfoOpenId(String openid);

    /**
     * 用户认证
     * @param userId
     * @param userAuthVo
     */
    void userAuth(Long userId, UserAuthVo userAuthVo);

    /**
     * 用户列表（条件查询带分页）
     * @param pageParam
     * @param userInfoQueryVo
     * @return
     */
    IPage<UserInfo> selectPage(Page<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo);

    /**
     * 用户锁定
     * @param userId
     * @param status
     */
    void lock(Long userId, Integer status);

    /**
     * 用户详情
     * @param userId
     * @return
     */
    Map<String, Object> show(Long userId);

    /**
     * 用户认证
     * @param userId
     * @param authStatus
     */
    void approval(Long userId, Integer authStatus);
}
