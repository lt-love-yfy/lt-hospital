package com.lt.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lt.hospital.model.user.UserInfo;


public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
