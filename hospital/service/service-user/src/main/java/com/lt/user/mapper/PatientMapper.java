package com.lt.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lt.hospital.model.user.Patient;

public interface PatientMapper extends BaseMapper<Patient> {
}
