package com.lt.msm.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConstantPropertiesUtils implements InitializingBean {

//    accountSid= ;
//    说明：主账号，登陆云通讯网站后，可在控制台首页看到开发者主账号ACCOUNT SID。
//
//    accountToken= ;
//    说明：主账号Token，登陆云通讯网站后，可在控制台首页看到开发者主账号AUTH TOKEN。
//
//    appId=;
//    说明：请使用管理控制台中已创建应用的APPID。



    //利用注解读取到配置信息当中的值
    @Value("${cloopen.sms.accountSId}")
    private String accountSId;
    @Value("${cloopen.sms.accountToken}")
    private String accountToken;
    @Value("${cloopen.sms.appId}")
    private String appId;

    public static String ACCOUNT_SID;
    public static String ACCOUNT_TOKEN;
    public static String APP_ID;

    @Override
    public void afterPropertiesSet() throws Exception {
        ACCOUNT_SID = accountSId;
        ACCOUNT_TOKEN =accountToken;
        APP_ID = appId;
    }
}
