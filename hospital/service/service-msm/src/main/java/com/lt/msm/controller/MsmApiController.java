package com.lt.msm.controller;


import com.lt.common.result.Result;
import com.lt.msm.service.MsmService;
import com.lt.msm.utils.RandomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@Api(tags = "发送手机短信")
@RestController
@RequestMapping("api/msm")
public class MsmApiController {

    @Autowired
    private MsmService msmService;

    //存储在redis当中，写一些短信有效时间等等
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @GetMapping("send/{phone}")
    @ApiOperation(value = "发送验证码")
    public Result sendCode(@PathVariable String phone) {
        //首先，我们需要从redis当中获取到手机的验证码，
        //key=手机号 value=验证码
        String code = redisTemplate.opsForValue().get(phone);
        //假如获取不到验证码
        if(!StringUtils.isEmpty(code)){
            return Result.ok();
        }

        // 生成六位验证码，
        code = RandomUtil.getSixBitRandom();
        //调用service方法，通过整合短信服务进行发送
        boolean isSend = msmService.send(phone,code);
        //生成验证码放到redis里面，设置有效时间
        if(isSend) {
            //phone 代表手机号，code为验证码，2是时间，最后一个代表单位（验证码两分钟过期时间）
            redisTemplate.opsForValue().set(phone,code,2, TimeUnit.MINUTES);
            return Result.ok();
        } else {
            return Result.fail().message("发送短信失败");
        }

    }
}
