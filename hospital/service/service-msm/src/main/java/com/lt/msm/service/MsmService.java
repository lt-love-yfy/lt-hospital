package com.lt.msm.service;

public interface MsmService {
    /**
     * 手机验证码发送
     * @param phone
     * @param code
     * @return
     */
    boolean send(String phone, String code);
}
