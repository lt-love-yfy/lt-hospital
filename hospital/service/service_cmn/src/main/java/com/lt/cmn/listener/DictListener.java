package com.lt.cmn.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.lt.cmn.mapper.DictMapper;
import com.lt.hospital.model.cmn.Dict;
import com.lt.hospital.vo.cmn.DictEeVo;
import org.springframework.beans.BeanUtils;


public class DictListener extends AnalysisEventListener<DictEeVo> {
    /**
     * 传入的数据，从第二行传入，因为第一行是字段信息
     * @param dictEeVo 对象信息
     * @param analysisContext
     */

    private DictMapper dictMapper;


    public DictListener(DictMapper dictMapper) {
        this.dictMapper = dictMapper;
    }

    @Override
    public void invoke(DictEeVo dictEeVo, AnalysisContext analysisContext) {
        Dict dict = new Dict();
        BeanUtils.copyProperties(dictEeVo,dict);
        dictMapper.insert(dict);//插入一条信息
    }

    /**
     * 传入的字段信息
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
