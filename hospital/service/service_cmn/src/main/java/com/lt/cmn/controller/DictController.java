package com.lt.cmn.controller;

import com.lt.cmn.service.DictService;
import com.lt.common.result.Result;
import com.lt.hospital.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(tags = "数据字典接口")
@RestController
@RequestMapping("/admin/cmn/dict")
//因为有了crossConfig配置了，这个注解可以去掉@CrossOrigin //跨域操作，因为前端的端口号和后端不一致，需要加入这个注解，（了解同源的概念）
public class DictController {

    @Autowired
    private DictService dictService;

    @ApiOperation(value = "根据数据id查询子数据列表")
    @GetMapping("queryChildId/{id}")
    public Result queryChildId (@PathVariable Long id){
        List<Dict> list = dictService.findChildId(id);
        return  Result.ok(list);
    }

    @ApiOperation(value = "根据数据dict_code查询子数据列表")
    @GetMapping("queryByDictCode/{dictCode}")
    public Result queryByDictCode (@PathVariable String dictCode){
        List<Dict> list = dictService.queryByDictCode(dictCode);
        return  Result.ok(list);
    }
    @ApiOperation(value="数据字典导出")
    @GetMapping("exportDict")
    //这里下面就不要写返回result了 不然控制台会报错
    public void exportDict(HttpServletResponse httpServletResponse){
        dictService.exportDictData(httpServletResponse);
    }


    @ApiOperation(value="数据字典导入")
    @PostMapping("importDict")
    public Result importDict(MultipartFile file){//MultipartFile 这个就是将文件导入的方法,也不知道为啥这个实参名必须为file
        dictService.importDictData(file);
        return Result.ok();
    }

    @ApiOperation(value="根据类型和值进行查询")
    @GetMapping("getName/{dictCode}/{value}")
    public String getName(@PathVariable("dictCode")String dictCode,
                          @PathVariable("value") String value){
        String dictName = dictService.getDictName(dictCode,value);
        return dictName;
    }

    @ApiOperation(value="根据值进行查询")
    @GetMapping("getName/{value}")
    public String getName(@PathVariable("value") String value){
        String dictName = dictService.getDictName("",value);
        return dictName;
    }

}
