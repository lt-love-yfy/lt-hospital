package com.lt.cmn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lt.hospital.model.cmn.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

//这里相当于mybatis plus帮忙写好了，继承了一个IService泛型 里面填写的就是那个实体类
public interface DictService extends IService<Dict> {
    /**
     * 根据数据id查询子数据列表
     * @param id
     * @return
     */
    List<Dict> findChildId(Long id);

    /**
     * 导出数据字典数据
     * @param httpServletResponse
     */
    void exportDictData(HttpServletResponse httpServletResponse);

    /**
     * 导入数据字典数据
     * @param file
     */
    void importDictData(MultipartFile file);

    /**
     * 查询名称
     * @param dictCode
     * @param value
     * @return
     */
    String getDictName(String dictCode, String value);

    /**
     * 根据dictCode查询下级节点数据
     * @param dictCode
     * @return
     */
    List<Dict> queryByDictCode(String dictCode);
}
