package com.lt.hospital.model.hosp;

import com.lt.hospital.model.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * HospitalSet
 * </p>
 *
 * @author qy
 */
@Data
//使用在被 @ApiModel 注解的模型类的属性上
@ApiModel(description = "医院设置")
//这个是mybatis plus的注解 表示的是表名 一般我们写在类上
@TableName("hospital_set")
public class HospitalSet extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
//  这些都是swagger注解当中的东西
//	@ApiModelProperty()用于方法，字段； 表示对model属性的说明或者数据操作更改
//	value–字段说明
//	name–重写属性名字
//	dataType–重写属性类型
//	required–是否必填
//	example–举例说明
//	hidden–隐藏
	@ApiModelProperty(value = "医院名称")
//  这也是mybatis plus的注解 如果里面写了（exist=false）表示当前属性不是数据库的字段，但在项目中必须使用，这样在使用bean的时候，mybatis-plus就会忽略这个，不会报错
	@TableField("hosname")
	private String hosname;

	@ApiModelProperty(value = "医院编号")
	@TableField("hoscode")
	private String hoscode;

	@ApiModelProperty(value = "api基础路径")
	@TableField("api_url")
	private String apiUrl;

	@ApiModelProperty(value = "签名秘钥")
	@TableField("sign_key")
	private String signKey;

	@ApiModelProperty(value = "联系人姓名")
	@TableField("contacts_name")
	private String contactsName;

	@ApiModelProperty(value = "联系人手机")
	@TableField("contacts_phone")
	private String contactsPhone;

	@ApiModelProperty(value = "状态")
	@TableField("status")
	private Integer status;

}

