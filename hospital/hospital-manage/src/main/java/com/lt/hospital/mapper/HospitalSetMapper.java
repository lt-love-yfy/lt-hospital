package com.lt.hospital.mapper;

import com.lt.hospital.model.HospitalSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
