package com.lt.cmn.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


//这个注解作用就是添加远程调用的那个模块 这是nacos下的
//spring.application.name=service-cmn
@FeignClient("service-cmn")
@Repository
public interface DictFeignClient {

    //就是把service-cmn下controller层当中需要的方法名拿来就行了,但是路径需要我们补全，就是端口号后面的路径
    //@PathVariable("") 这个需要补全

    @GetMapping("/admin/cmn/dict/getName/{dictCode}/{value}")
    String getName(@PathVariable("dictCode") String dictCode,@PathVariable("value") String value);

    @GetMapping("/admin/cmn/dict/getName/{value}")
    String getName(@PathVariable("value") String value);

}
