import request from '@/utils/request'

const api_name = `/api/msm`

export default {
    //短信服务发送
    sendCode(mobile) {
        return request({
            url: `${api_name}/send/${mobile}`,
            method: `get`
        })
    }
}