import request from '@/utils/request'

const api_name = `/api/hospital/user`

export default {
  getPageList(page, limit, searchObj) {
    return request({
      url: `${api_name}/findHospitalList/${page}/${limit}`,
      method: 'get',
      params: searchObj
    })
  },
  getByHosname(hosname) {
    return request({
      url: `${api_name}/findHospitalByName/${hosname}`,
      method: 'get'
    })
  },
  //根据编号查询医院信息
  show(hoscode) {
    return request({
      url: `${api_name}/findHospitalDetail/${hoscode}`,
      method: 'get'
    })
  },
  //根据医院编号查询科室
  findDepartment(hoscode) {
    return request({
      url: `${api_name}/department/${hoscode}`,
      method: 'get'
    })
  },
  //获取可预约排班数据
  getBookingScheduleRule(page, limit, hoscode, depcode) {
    return request({
      url: `${api_name}/auth/getBookingScheduleRule/${page}/${limit}/${hoscode}/${depcode}`,
      method: 'get'
    })
  },
  //获取排班数据      
  findScheduleList(hoscode, depcode, workDate) {
    return request({
      url: `${api_name}/auth/findScheduleList/${hoscode}/${depcode}/${workDate}`,
      method: 'get'
    })
  },
  //根据排班id获取具体排班的信息
  getSchedule(id) {
    return request({
      url: `${api_name}/getSchedule/${id}`,
      method: 'get'
    })
  }
}