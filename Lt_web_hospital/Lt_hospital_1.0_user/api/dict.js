import request from '@/utils/request'

const api_name  = `/admin/cmn/dict`

export default{
    //根据数据dict_code查询子数据列表
    queryByDictCode(dictCode){
        return request({
            url:`${api_name}/queryByDictCode/${dictCode}`,
            method:'get'
        })
    },
    //根据数据id查询子数据列表
    queryChildId(id){
        return request({
            url:`${api_name}/queryChildId/${id}`,
            method:'get'
        })
    },
}