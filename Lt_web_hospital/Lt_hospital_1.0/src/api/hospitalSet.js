import request from '@/utils/request' //这里就直接使用了封装好的axios

export default{
  //分页查询方法
  getHospiatalPageList(current,limit,searchObj){//定义方法名，参数为当前页，每页显示条数，搜索的条件（这是json传送）
    return request({ //返回的必须是上面导入的request通过ajax请求
      url: `/admin/hospital/hospitalSet/queryPageHospital/${current}/${limit}`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'post',//这是提交的方法
      data:searchObj //因为是json的形式传递，所以使用data
  })
  },
  //删除数据
  removeById(id){
    return request({
      url: `/admin/hospital/hospitalSet/${id}`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'delete'//这是提交的方法
    })
  },
  //批量删除数据
  batchRemoveHospitalSet(idlist){
    return request({
      url: `/admin/hospital/hospitalSet/batchRemoveHospitalSet`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'delete',//这是提交的方法
      data:idlist
    })
  },
  //锁定与取消锁定
  lockHospitalSet(id,status){
    return request({
      url: `/admin/hospital/hospitalSet/lockHospitalSet/${id}/${status}`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'put',//这是提交的方法
    })
  },
  //添加医院的操作
  addHospitalSet(hospitalSet){
    return request({
      url: `/admin/hospital/hospitalSet/addHospitalSet`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'post',//这是提交的方法
      data:hospitalSet
    })
  },
  //根据id查询医院数据
  queryHospitalSetById(id){
    return request({
      url: `/admin/hospital/hospitalSet/queryHospitalSetById/${id}`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'get',//这是提交的方法
    })
  },
  //修改医院信息
  updateHospitalSet(hospitalSet){
    return request({
      url: `/admin/hospital/hospitalSet/updateHospitalSet`,//这个利用反引号括起来，里面填写后端写额路径值
      method: 'post',//这是提交的方法
      data:hospitalSet
    })
  },
}
