import request from '@/utils/request' //这里就直接使用了封装好的axios

export default{
    //数据字典列表
    dictList(id) {
        return request({
            url: `/admin/cmn/dict/queryChildId/${id}`,    
            method :'get'    
        })
    }
}