import request from '@/utils/request' //这里就直接使用了封装好的axios

export default{
    //医院信息列表
    getHospiralList(page,limit,searchObj) {
        return request({
            url: `/admin/hospital/queryPageMyHospital/${page}/${limit}`,    
            method: 'get',
            params:searchObj //如果没有requestbody 传来的数据用params接收    
        })
    },
    //根据数据dict_code查询子数据列表
    queryByDictCode(dictCode) {
        return request({
            url: `/admin/cmn/dict/queryByDictCode/${dictCode}`,    
            method: 'get', 
        })
    },
    //根据数据id查询子数据列表
    queryChildId(id) {
        return request({
            url: `/admin/cmn/dict/queryChildId/${id}`,    
            method: 'get',  
        })
    },
    //更改医院上线状态
    updateHospitalStatus(id,status) {
        return request({
            url: `/admin/hospital/updateHospitalStatus/${id}/${status}`,    
            method: 'post',  
        })
    },
    //查看医院详情信息
    showHospitalDetail(id){
        return request({
            url: `/admin/hospital/showHospitalDetail/${id}`,    
            method: 'get', 
        })
    },
    //查看科室详情
    departmentList(hoscode){
        return request({
            url: `/admin/hospital/department/departmentList/${hoscode}`,    
            method: 'get', 
        })
    },
    //查看排班详情
    scheduleList(page,limit,hoscode,depcode){
        return request({
            url: `/admin/hospital/schedule/scheduleList/${page}/${limit}/${hoscode}/${depcode}`,    
            method: 'get', 
        })
    },
    //查看排班具体信息详情
    scheduleDetailList(hoscode,depcode,workDate){
        return request({
            url: `/admin/hospital/schedule/scheduleDetailList/${hoscode}/${depcode}/${workDate}`,    
            method: 'get', 
        })
    }

}