# 尚医通医院预约挂号

#### 介绍
尚硅谷，预约挂号尚医通项目，用户通过网上定位所在医院科室，进行网上预约挂号进行咨询的一种看病方式。分为前台预约界面和后台管理界面，前台对用户开放，后台数据审核管理进行批准，上传给前台相关医院科室预约信息。

#### 技术架构
SpringBoot+SpringCloud+MyBatis-Plus+Redis+RabbitMQ+HTTPClient+Swagger2+Vue+Git
前端管理界面在集成的vue-admin-template-master模板搭建
前端交互界面在集成的轻量型nuxt模板搭建
原有课程中的阿里云不在使用，用容联云代替短信发送验证码


#### 服务架构
![输入图片说明](pictures/image01.png)

#### 界面展示
![输入图片说明](pictures/image02.png)
![输入图片说明](pictures/image03.png)
![输入图片说明](pictures/image04.png)
![输入图片说明](pictures/image05.png)